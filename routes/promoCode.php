<?php

use App\Http\Controllers\PromoCodeController;
use Illuminate\Support\Facades\Route;

Route::group([
    'middleware' => [],
    'prefix' => '/promo-code/'
], function () {
    Route::post('generate', [PromoCodeController::class, 'generate'])->name('promoCode.generateEndPoint');
    Route::get('all', [PromoCodeController::class, 'getAll'])->name('promoCode.getAllEndPoint');
    Route::post('validate', [PromoCodeController::class, 'checkValidity'])->name('promoCode.checkValidityEndPoint');
});
