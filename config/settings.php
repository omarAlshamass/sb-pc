<?php
return [
    'itemsPerPage' => 15,
    'google_public_key' => env('GOOGLE_PUBLIC_KEY')
];
