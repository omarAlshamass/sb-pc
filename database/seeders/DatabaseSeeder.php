<?php

namespace Database\Seeders;

use App\Models\Client;
use App\Models\Event;
use App\Models\PromoCode;
use Carbon\Carbon;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $event = Event::factory()->create(['longitude' => 33.556549, 'latitude' => 36.353828]);
        Client::factory()->create(['id' => 1]);
        PromoCode::factory()->create([
            'expiry_date' => Carbon::now()->addMonth(),
            'maximum_amount' => 30000,
            'maximum_number_of_usages' => 10,
            'is_active' => true,
            'event_id' => $event->id,
            'code' => 'D2NooMTR'
        ]);
    }
}
