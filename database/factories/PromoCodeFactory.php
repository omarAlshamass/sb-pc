<?php

namespace Database\Factories;

use App\Models\Event;
use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\PromoCode>
 */
class PromoCodeFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        return [
            'name' => $this->faker->name(),
            'code' => Str::random(20),
            'maximum_amount' => $this->faker->randomDigitNotNull(),
            'maximum_number_of_usages' => $this->faker->randomDigit(),
            'expiry_date' => $this->faker->dateTimeThisMonth(),
            'is_active' => $this->faker->boolean(),
            'event_id' => Event::factory()->create(),
            'allowed_radius_for_free_usages' => $this->faker->biasedNumberBetween(200, 500)
        ];
    }
}
