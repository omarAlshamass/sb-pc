<?php

namespace Tests\Feature\Apis;

use App\Models\Client;
use App\Models\Event;
use App\Models\PromoCode;
use Carbon\Carbon;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class PromoCodeValidityAPITests extends TestCase
{
    use RefreshDatabase;

    public function testMakeApiCallWithoutPayload()
    {
        $response = $this->postJson(route('promoCode.checkValidityEndPoint'), []);
        $responseBody = $response->json('errors');

        $this->assertArrayHasKey('promo_code', $responseBody);
        $this->assertArrayHasKey('origin.latitude', $responseBody);
        $this->assertArrayHasKey('origin.longitude', $responseBody);
        $this->assertArrayHasKey('destination.latitude', $responseBody);
        $this->assertArrayHasKey('destination.longitude', $responseBody);
        $response->assertStatus(422);
    }

    public function testMakeApiCallWithInvalidOriginCoordinates()
    {
        $promoCodeObject = PromoCode::factory()->create([
            'expiry_date' => Carbon::now()->addMonth(),
            'maximum_amount' => 30000,
            'maximum_number_of_usages' => 10,
            'is_active' => true
        ]);
        $response = $this->postJson(route('promoCode.checkValidityEndPoint', [
            'promo_code' => $promoCodeObject->code,
            'origin' => [
                'latitude' => 35.205212,
                'longitude' => 33.997748,
            ],
            'destination' => [
                'latitude' => 36.306073,
                'longitude' => 33.537710
            ]
        ]));

        $this->assertSame($response->json('error'), "No data regarding your current inputs, please try fixing them and try calling the API again.");
        $response->assertStatus(422);
    }

    public function testMakeApiCallWithInvalidDestinationCoordinates()
    {
        $promoCodeObject = PromoCode::factory()->create([
            'expiry_date' => Carbon::now()->addMonth(),
            'maximum_amount' => 30000,
            'maximum_number_of_usages' => 10,
            'is_active' => true
        ]);
        $response = $this->postJson(route('promoCode.checkValidityEndPoint', [
            'promo_code' => $promoCodeObject->code,
            'origin' => [
                'latitude' => 36.306073,
                'longitude' => 33.537710
            ],
            'destination' => [
                'latitude' => 35.205212,
                'longitude' => 33.997748,
            ]
        ]));

        $this->assertSame($response->json('error'), "No data regarding your current inputs, please try fixing them and try calling the API again.");
        $response->assertStatus(422);
    }

    public function testMakeApiCallWithValidOriginAndDestinationCoordinates()
    {
        $event = Event::factory()->create(['longitude' => 33.556549, 'latitude' => 36.353828]);
        Client::factory()->create(['id' => 1]);
        $promoCodeObject = PromoCode::factory()->create([
            'expiry_date' => Carbon::now()->addMonth(),
            'maximum_amount' => 30000,
            'maximum_number_of_usages' => 10,
            'is_active' => true,
            'event_id' => $event->id
        ]);
        $response = $this->postJson(route('promoCode.checkValidityEndPoint', [
            'promo_code' => $promoCodeObject->code,
            'origin' => [
                'latitude' => 36.352942,
                'longitude' => 33.557046
            ],
            'destination' => [
                'latitude' => 36.351503,
                'longitude' => 33.557535
            ]
        ]));

        $responseBody = $response->json('data');

        $this->assertSame(data_get($responseBody, 'promo_code.name'), $promoCodeObject->name);
        $this->assertArrayHasKey('polyline', $responseBody);
        $this->assertNotNull(data_get($responseBody, 'polyline.0.end_address'));
        $this->assertNotNull(data_get($responseBody, 'polyline.0.start_location'));
        $response->assertStatus(200);
    }

    public function testMakeApiCallWithInvalidAmountOfPromoCode()
    {
        $promoCodeObject = PromoCode::factory()->create([
            'expiry_date' => Carbon::now()->addMonth(),
            'maximum_amount' => 0,
            'maximum_number_of_usages' => 10,
            'is_active' => true
        ]);
        $response = $this->postJson(route('promoCode.checkValidityEndPoint', [
            'promo_code' => $promoCodeObject->code,
            'origin' => [
                'latitude' => 36.352942,
                'longitude' => 33.557046
            ],
            'destination' => [
                'latitude' => 36.351503,
                'longitude' => 33.557535
            ]
        ]));

        $this->assertEquals($response->json('error'), 'The selected promo code is invalid.');
        $response->assertStatus(422);
    }

    public function testMakeApiCallWithInvalidMaximumNumberOfUsagesOfPromoCode()
    {
        $promoCodeObject = PromoCode::factory()->create([
            'expiry_date' => Carbon::now()->addMonth(),
            'maximum_amount' => 10000,
            'maximum_number_of_usages' => 0,
            'is_active' => true
        ]);
        $response = $this->postJson(route('promoCode.checkValidityEndPoint', [
            'promo_code' => $promoCodeObject->code,
            'origin' => [
                'latitude' => 36.352942,
                'longitude' => 33.557046
            ],
            'destination' => [
                'latitude' => 36.351503,
                'longitude' => 33.557535
            ]
        ]));

        $this->assertEquals($response->json('error'), 'The selected promo code is invalid.');
        $response->assertStatus(422);
    }

    public function testMakeApiCallWithInvalidExpiryDateOfPromoCode()
    {
        $promoCodeObject = PromoCode::factory()->create([
            'expiry_date' => Carbon::now()->subMonth(),
            'maximum_amount' => 10000,
            'maximum_number_of_usages' => 0,
            'is_active' => true
        ]);
        $response = $this->postJson(route('promoCode.checkValidityEndPoint', [
            'promo_code' => $promoCodeObject->code,
            'origin' => [
                'latitude' => 36.352942,
                'longitude' => 33.557046
            ],
            'destination' => [
                'latitude' => 36.351503,
                'longitude' => 33.557535
            ]
        ]));

        $this->assertEquals($response->json('error'), 'The selected promo code is invalid.');
        $response->assertStatus(422);
    }

    public function testMakeApiCallWithInActivePromoCode()
    {
        $promoCodeObject = PromoCode::factory()->create([
            'expiry_date' => Carbon::now()->addMonth(),
            'maximum_amount' => 10000,
            'maximum_number_of_usages' => 0,
            'is_active' => false
        ]);
        $response = $this->postJson(route('promoCode.checkValidityEndPoint', [
            'promo_code' => $promoCodeObject->code,
            'origin' => [
                'latitude' => 36.352942,
                'longitude' => 33.557046
            ],
            'destination' => [
                'latitude' => 36.351503,
                'longitude' => 33.557535
            ]
        ]));

        $this->assertEquals($response->json('error'), 'The selected promo code is invalid.');
        $response->assertStatus(422);
    }
}
