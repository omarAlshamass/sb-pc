<?php

namespace Tests\Feature\Apis;

use App\Models\PromoCode;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class PromoCodeGetAllAPITests extends TestCase
{
    use RefreshDatabase;

    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testMakeApiCallWithEmptyResponse()
    {
        $response = $this->getJson(route('promoCode.getAllEndPoint', []));
        $responseBody = $response->json('data');

        $this->assertEquals(data_get($responseBody, 'current_page'), 1);
        $this->assertEquals(data_get($responseBody, 'per_page'), config("settings.itemsPerPage"));
        $this->assertEquals(data_get($responseBody, 'data'), []);
        $response->assertStatus(200);
    }

    public function testMakeApiCallWithActualResponse()
    {
        $promoCodes = PromoCode::factory()->count(100)->create();

        $response = $this->getJson(route('promoCode.getAllEndPoint', []));
        $responseBody = $response->json('data');

        $this->assertEquals(count(data_get($responseBody, 'data')), config("settings.itemsPerPage"));
        $this->assertEquals(data_get($responseBody, 'total'), count($promoCodes));
        $this->assertNotEmpty(data_get($responseBody, 'last_page'));
        $this->assertNotEmpty(data_get($responseBody, 'last_page_url'));
        $this->assertNotEmpty(data_get($responseBody, 'next_page_url'));
        $response->assertStatus(200);
    }

    public function testMakeApiCallWithActualResponseFilteringActiveIsTrueButNoMatchingData()
    {
        $promoCodes = PromoCode::factory()->count(100)->create(['is_active' => false]);

        $response = $this->getJson(route('promoCode.getAllEndPoint', ['active' => 'true']));
        $responseBody = $response->json('data');

        $this->assertEquals(count($promoCodes), 100);
        $this->assertEquals(count($promoCodes->where(fn ($item) => $item->is_active == true)), 0);
        $this->assertEquals(data_get($responseBody, 'data'), []);
        $this->assertEquals(data_get($responseBody, 'total'), 0);
        $this->assertNull(data_get($responseBody, 'error'));
        $response->assertStatus(200);
    }

    public function testMakeApiCallWithActualResponseFilteringActiveIsTrueWithMatchingData()
    {
        PromoCode::factory()->count(100)->create();

        $response = $this->getJson(route('promoCode.getAllEndPoint', ['active' => 'true']));
        $responseBody = $response->json('data');

        $this->assertNotEmpty(data_get($responseBody, 'data'));
        $this->assertNotEquals(data_get($responseBody, 'total'), 100);
        $this->assertNull(data_get($responseBody, 'error'));
        $response->assertStatus(200);
    }

    public function TestMakingAPICallWitPageAndActiveAsQueryParams()
    {
        PromoCode::factory()->count(100)->create();

        $response = $this->getJson(route('promoCode.getAllEndPoint', ['active' => 'true', 'page' => 3]));
        $responseBody = $response->json('data');

        $this->assertNotEquals(data_get($responseBody, 'total'), 100);
        $this->assertEquals(data_get($responseBody, 'current_page'), 3);
        $this->assertNull(data_get($responseBody, 'error'));
        $this->assertNotEmpty(data_get($responseBody, 'data'));
        $this->assertNotEmpty(data_get($responseBody, 'last_page'));
        $this->assertNotEmpty(data_get($responseBody, 'last_page_url'));
        $this->assertNotEmpty(data_get($responseBody, 'next_page_url'));
        $response->assertStatus(200);
    }
}
