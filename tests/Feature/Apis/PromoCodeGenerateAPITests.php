<?php

namespace Tests\Feature\Apis;

use App\Models\Event;
use App\Models\PromoCode;
use Carbon\Carbon;
use Illuminate\Support\Str;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class PromoCodeGenerateAPITests extends TestCase
{
    use RefreshDatabase;

    public function testMakeApiCallWithoutPayload()
    {
        $response = $this->postJson(route('promoCode.generateEndPoint', []));
        $responseBody = $response->json('errors');

        $this->assertArrayHasKey('promo_code_count', $responseBody);
        $this->assertArrayHasKey('data.name', $responseBody);
        $this->assertArrayHasKey('data.maximum_amount', $responseBody);
        $this->assertArrayHasKey('data.maximum_number_of_usages', $responseBody);
        $this->assertArrayHasKey('data.expiry_date', $responseBody);
        $this->assertArrayHasKey('data.allowed_radius_for_free_usages', $responseBody);
        $response->assertStatus(422);
    }

    public function testMakeApiCallWithPayload()
    {
        $eventObject = Event::factory()->create();
        $newPromoCodesCount = 300;
        $response = $this->postJson(route('promoCode.generateEndPoint', [
            'promo_code_count' => $newPromoCodesCount,
            'data' => [
                'name' => Str::random(10),
                'maximum_amount' => 10000,
                'maximum_number_of_usages' => 10,
                'expiry_date' => (string) Carbon::now()->endOfDay(),
                'allowed_radius_for_free_usages' => 400,
                'event_id' => $eventObject->id
            ]
        ]));

        $this->assertEquals(PromoCode::count(), $newPromoCodesCount);
        $response->assertStatus(200);
    }
}
