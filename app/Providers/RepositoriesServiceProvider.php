<?php

namespace App\Providers;

use App\Repositories\Interfaces\EventRepositoryInterface;
use App\Repositories\Interfaces\PromoCodeRepositoryInterface;
use App\Repositories\PromoCodeRepository;
use App\Repositories\EventRepository;
use App\Repositories\Interfaces\PromoCodeConsumptionHistoryRepositoryInterface;
use App\Repositories\PromoCodeConsumptionHistoryRepository;
use Illuminate\Support\ServiceProvider;

class RepositoriesServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $this->app->bind(EventRepositoryInterface::class, EventRepository::class);
        $this->app->bind(PromoCodeRepositoryInterface::class, PromoCodeRepository::class);
        $this->app->bind(PromoCodeConsumptionHistoryRepositoryInterface::class, PromoCodeConsumptionHistoryRepository::class);
    }
}
