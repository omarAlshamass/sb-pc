<?php

namespace App\Providers;

use App\Libraries\Map\GoogleMap;
use App\Libraries\Map\MapAbstract;
use Illuminate\Support\ServiceProvider;

class LibrariesServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $this->app->bind(MapAbstract::class, GoogleMap::class);
    }
}
