<?php

namespace App\Libraries\Map;

abstract class MapAbstract
{
    public abstract function getDrivingDistance($pointA, $pointB);

    public abstract function getPolyLines($pointA, $pointB);
}
