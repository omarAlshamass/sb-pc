<?php

namespace App\Libraries\Map;

use App\Exceptions\APICallFailedException;
use App\Exceptions\APICallNoResponseException;
use App\Libraries\Map\MapAbstract;

class GoogleMap extends MapAbstract
{
    public function getDrivingDistance($pointA, $pointB)
    {
        ['latitude' => $latitudeA, 'longitude' => $longitudeA] = $pointA;
        ['latitude' => $latitudeB, 'longitude' => $longitudeB] = $pointB;

        $url = "https://maps.googleapis.com/maps/api/distancematrix/json?origins=" . $latitudeA . "," . $longitudeA . "&destinations=" . $latitudeB . "," . $longitudeB . "&mode=driving&language=pl-PL&key=" . config('settings.google_public_key');
        $response = $this->makeAPICall($url);

        return data_get($response, 'rows.0.elements.0.distance.value') ?? throw new APICallNoResponseException();
    }

    public function getPolyLines($pointA, $pointB)
    {
        ['latitude' => $latitudeA, 'longitude' => $longitudeA] = $pointA;
        ['latitude' => $latitudeB, 'longitude' => $longitudeB] = $pointB;

        $url = "https://maps.googleapis.com/maps/api/directions/json?origin=" . $latitudeA . "," . $longitudeA . "&destination=" . $latitudeB . "," . $longitudeB . "&key=" . config('settings.google_public_key');
        $response = $this->makeAPICall($url);

        return data_get($response, 'routes.0.legs') ?? throw new APICallNoResponseException();
    }

    private function makeAPICall($url)
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_PROXYPORT, 3128);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_close($ch);

        $response = json_decode(curl_exec($ch), true);

        return data_get($response, 'status') ? $response : throw new APICallFailedException();
    }
}
