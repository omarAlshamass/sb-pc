<?php

namespace App\Http\Requests;

use Illuminate\Validation\Rule;
use Illuminate\Foundation\Http\FormRequest;

class GetAllPromoCodesRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        #TODO: make it only available for admins
        return true;
    }

    public function all($keys = null)
    {
        $data = parent::all($keys);
        $data['active'] = $this->query('active');

        return $data;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'active' => ['nullable', Rule::in(['true', 'false'])]
        ];
    }
}
