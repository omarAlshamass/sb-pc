<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class generatePromoCodesRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        #TODO: this action should be accessable only by admins
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'promo_code_count' => 'required|integer',
            'data.name' => 'required|string',
            'data.maximum_amount' => 'required|integer',
            'data.maximum_number_of_usages' => 'required|integer',
            'data.expiry_date' => 'required|date',
            'data.event_id' => 'required|exists:events,id',
            'data.allowed_radius_for_free_usages' => 'required|integer'
        ];
    }
}
