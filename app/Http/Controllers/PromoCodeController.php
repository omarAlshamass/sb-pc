<?php

namespace App\Http\Controllers;

use App\Http\Requests\CheckPromoCodeValidityRequest;
use App\Http\Requests\GeneratePromoCodesRequest;
use App\Http\Requests\GetAllPromoCodesRequest;
use App\Jobs\ProccessGeneratingPromoCodes;
use App\Repositories\Interfaces\PromoCodeRepositoryInterface;
use App\Services\PromoCodeService;

class PromoCodeController extends Controller
{
    public function __construct(private PromoCodeService $promoCodeService)
    {
    }

    public function getAll(GetAllPromoCodesRequest $request)
    {
        return $this->promoCodeService->getAll($request->validated());
    }

    public function generate(generatePromoCodesRequest $request)
    {
        return $this->promoCodeService->generate($request->validated());
    }

    public function checkValidity(CheckPromoCodeValidityRequest $request)
    {
        return $this->promoCodeService->checkValidity($request->validated());
    }
}
