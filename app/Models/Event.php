<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Event extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
        'address',
        'longitude',
        'latitude'
    ];

    public function promoCodes()
    {
        return $this->hasMany(PromoCode::class);
    }

    public function getCoordinates()
    {
        return collect(['longitude' => $this->longitude, 'latitude' => $this->latitude]);
    }
}
