<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PromoCodeConsumptionHistory extends Model
{
    use HasFactory;

    protected $table = 'promo_codes_consumption_history';
    protected $fillable = [
        'promo_code_id',
        'client_id',
        'consuming_date',
        'amount'
    ];

    public function client()
    {
        return $this->belongsTo(Client::class);
    }

    public function PromoCode()
    {
        return $this->belongsTo(PromoCode::class);
    }
}
