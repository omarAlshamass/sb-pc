<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PromoCode extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
        'code',
        'maximum_amount',
        'maximum_number_of_usages',
        'expiry_date',
        'is_active',
        'event_id',
        'allowed_radius_for_free_usages'
    ];

    public function event()
    {
        return $this->belongsTo(Event::class);
    }

    public function consumptions()
    {
        return $this->hasMany(PromoCodeConsumptionHistory::class);
    }
}
