<?php

namespace App\Helpers;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Str;

class PromoCodeHelper
{
    public static function getRandomCodes($number)
    {
        return Collection::times($number, fn () => Str::random(8));
    }
}
