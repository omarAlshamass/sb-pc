<?php

namespace App\Helpers;

class BaseResourceHelper
{
    public static function ok()
    {
        return response()->json([
            'message' => 'successful',
            'code' => 200,
            'data' => null,
            'error' => null,
        ], 200);
    }

    public static function create($data)
    {
        return response()->json([
            'message' => 'successful',
            'code' => 200,
            'data' => $data,
            'error' => null
        ], 200);
    }

    public static function error($error, $data = [], $code = 422)
    {
        return response()->json([
            'message' => 'failed',
            'code' => 422,
            'data' => $data,
            'error' => $error
        ], $code);
    }
}
