<?php

namespace App\Repositories;

use App\Libraries\Map\MapAbstract;
use App\Models\PromoCode;
use App\Repositories\Interfaces\PromoCodeRepositoryInterface;

class PromoCodeRepository implements PromoCodeRepositoryInterface
{
    public function __construct(private MapAbstract $map)
    {
    }

    public function getAll($payload)
    {
        $active = data_get($payload, 'active', null);

        return PromoCode::when($active, fn ($q) => $q->where(['is_active' => $active]))
            ->paginate(config('settings.itemsPerPage'));
    }

    public function generate($payload, $codes)
    {
        $number = data_get($payload, 'promo_code_count');

        $affected = PromoCode::insertOrIgnore($codes->map(fn ($code) => [
            'code' => $code,
            'name' => data_get($payload, 'data.name'),
            'maximum_amount' => data_get($payload, 'data.maximum_amount'),
            'maximum_number_of_usages' => data_get($payload, 'data.maximum_number_of_usages'),
            'expiry_date' => data_get($payload, 'data.expiry_date'),
            'is_active' => true,
            'event_id' => data_get($payload, 'data.event_id'),
            'allowed_radius_for_free_usages' => data_get($payload, 'data.allowed_radius_for_free_usages')
        ])->toArray());

        if ($affected < $number) {
            $this->generate($number - $affected, $codes);
        }
    }

    public function getByCode($code)
    {
        return PromoCode::where(['code' => $code])->first();
    }
}
