<?php

namespace App\Repositories\Interfaces;

interface PromoCodeConsumptionHistoryRepositoryInterface
{
    public function insert($promoCodeId, $clientId, $amount);
}
