<?php

namespace App\Repositories\Interfaces;

interface PromoCodeRepositoryInterface
{
    public function getAll($payload);

    public function generate($payload, $codes);

    public function getByCode($code);
}
