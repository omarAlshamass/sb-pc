<?php

namespace App\Repositories;

use App\Models\PromoCodeConsumptionHistory;
use App\Repositories\Interfaces\PromoCodeConsumptionHistoryRepositoryInterface;

class PromoCodeConsumptionHistoryRepository implements PromoCodeConsumptionHistoryRepositoryInterface
{
    public function insert($promoCodeId, $clientId, $amount)
    {
        return PromoCodeConsumptionHistory::create([
            'promo_code_id' => $promoCodeId,
            'client_id' => $clientId,
            'amount' => $amount,
            'consuming_date' => (string) now()
        ]);
    }
}
