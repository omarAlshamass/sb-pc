<?php

namespace App\Services;

use App\Helpers\BaseResourceHelper;
use App\Jobs\ProccessGeneratingPromoCodes;
use App\Libraries\Map\MapAbstract;
use App\Repositories\Interfaces\PromoCodeConsumptionHistoryRepositoryInterface;
use App\Repositories\Interfaces\PromoCodeRepositoryInterface;

class PromoCodeService
{
    public function __construct(
        private MapAbstract $map,
        private PromoCodeRepositoryInterface $promoCodeRepository,
        private PromoCodeConsumptionHistoryRepositoryInterface $promoCodeConsumptionHistoryRepository
    ) {
    }

    public function getAll($payload)
    {
        return BaseResourceHelper::create($this->promoCodeRepository->getAll($payload));
    }

    public function generate($payload)
    {
        ProccessGeneratingPromoCodes::dispatch($payload);

        return BaseResourceHelper::ok();
    }

    public function checkValidity($payload)
    {
        ['promo_code' => $promoCode, 'origin' => $origin, 'destination' => $destination] = $payload;
        $promoCode = $this->promoCodeRepository->getByCode($promoCode);

        if (
            !$promoCode->is_active
            || $promoCode->expiry_date <= (string) now()
            || $promoCode->consumptions->sum('amount') >= $promoCode->maximum_amount
            || $promoCode->consumptions->count() >= $promoCode->maximum_number_of_usages
        )
            return BaseResourceHelper::error("The selected promo code is invalid.");

        $originEventDistance = $this->map->getDrivingDistance($origin, $promoCode->event->getCoordinates());
        $eventDistinationDistance = $this->map->getDrivingDistance($destination, $promoCode->event->getCoordinates());
        if (($promoCode->allowed_radius_for_free_usages < $originEventDistance && $promoCode->allowed_radius_for_free_usages < $eventDistinationDistance))
            return BaseResourceHelper::error("Your riding is out of the free tier");
        else {
            #TODO: There is no actual amount, so I assumed it is 1000
            $this->promoCodeConsumptionHistoryRepository->insert($promoCode->id, 1, 1000);
            return BaseResourceHelper::create([
                'promo_code' => $promoCode->first(),
                'polyline' => $this->map->getPolyLines($origin, $destination)
            ]);
        }
    }
}
