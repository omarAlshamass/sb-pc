<?php

namespace App\Exceptions;

use App\Helpers\BaseResourceHelper;
use Exception;

class APICallFailedException extends Exception
{
    public function render()
    {
        return BaseResourceHelper::error("An error happened during the API call, please try again later, Thanks.");
    }
}
