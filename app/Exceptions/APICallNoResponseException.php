<?php

namespace App\Exceptions;

use App\Helpers\BaseResourceHelper;
use Exception;

class APICallNoResponseException extends Exception
{
    public function render()
    {
        return BaseResourceHelper::error("No data regarding your current inputs, please try fixing them and try calling the API again.");
    }
}
